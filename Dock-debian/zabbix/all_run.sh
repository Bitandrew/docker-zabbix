#!/bin/bash

DIR=/usr/share/doc/zabbix-server-mysql
FILE="create.sql.gz"

#Create the file .my.cnf
if  [ ! -f /root/.my.cnf ];then 
  echo "Create the file .my.cnf in /root" 
  cat > /root/.my.cnf <<-EOF
[mysql]
user=zabbix
password=zabbixpasswd
host=zabbixdb
EOF
fi

# Checking to connect to mysql server
for i in {200..0} ; do

 if  mysql -Bse "select 1;" 2>/dev/null ; then
     break
 fi
    echo "ERROR: Mysql not reachable"
    sleep 0.5
done

if [ "$i" -eq 0 ];then
   echo "ERROR: Could not connect to Mysql server"
   exit 1
fi


# Check to tables are in the zabbixDB.

array=(`mysql -Bse "show tables from zabbix;"`)

if [ "${array[0]}" = "" ];then
   
   echo "Create tables in the zabbixDB"
   echo "Start creating tables.Its take few minutes" 
   echo "" 
   cd ${DIR} && zcat ${FILE} | mysql zabbix 
   
   echo "SQL finished"

else 

    echo "Tables have already created."

fi


if [ ! -f /ssh_pass ] ; then
    /ssh_pass.sh
fi

#Create directories in the /var/log.
if ! [ -d /var/log/nginx ] ;then
   mkdir /var/log/nginx 
fi

if  ! [ -d /var/log/zabbix ]; then
   mkdir /var/log/zabbix 
   chown zabbix:zabbix /var/log/zabbix 
fi

if ! [ -d /var/log/supervisor ]; then
   mkdir /var/log/supervisor
fi

#Run supervisord 
/usr/bin/supervisord -n -c /etc/supervisord.conf 
