#!/bin/bash
GREEN="\\e[42;5;5;82m"
END="\\e[0m"

if [ -f /.root_pass ] ; then 
   echo "Root password was set!"
   exit 0

fi

PASS=${PASS_FOR_ROOT:-$(openssl rand -base64 12)}
echo "root:$PASS" | chpasswd

echo "=> The root password has been set!"
touch /.root_pass

echo "========================================================================"
echo ""
echo -e "Passoword: ${GREEN} ${PASS} ${END}"
echo "You may change password after connect via ssh"
echo ""
echo "========================================================================"
