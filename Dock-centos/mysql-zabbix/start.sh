#!/bin/bash

if [ ! -f /ssh_pass ] ; then
    /ssh_pass.sh
fi

if [ ! -d /var/log/mysql ];then
   mkdir /var/log/mysql && chown mysql:mysql /var/log/mysql
fi

if [ ! -d /var/log/supervisor ]; then
    mkdir /var/log/supervisor 
fi


#Run supervisord 
/usr/bin/supervisord -n -c /etc/supervisord.conf 
